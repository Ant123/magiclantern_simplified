#ifndef HW_SCALER_H

#define HW_SCALER_H


// Supported filtering.
typedef enum FilterMode {
  kFilterNone = 0,      // Point sample; Fastest.
  kFilterLinear = 1,    // Filter horizontally only.
  kFilterBilinear = 2,  // Faster than box, but lower quality scaling down.
  kFilterBox = 3        // Highest quality.
} FilterModeEnum;

JRESULT jpegdec (uint8_t*, uint8_t* );
size_t input_func  (JDEC* , uint8_t* , size_t);
int output_func (JDEC* , void* , JRECT* );
void rgb_to_uyvy(uint8_t* , uint8_t*);
int uyvy_scale(uint8_t* , uint8_t*);

int UYVYToARGB(const uint8_t* src_uyvy,
               int src_stride_uyvy,
               uint8_t* dst_argb,
               int dst_stride_argb,
               int width,
               int height);
			   
int ARGBScale(const uint8_t* src_argb,
              int src_stride_argb,
              int src_width,
              int src_height,
              uint8_t* dst_argb,
              int dst_stride_argb,
              int dst_width,
              int dst_height,
              enum FilterMode filtering);

int ARGBToUYVY(const uint8_t* src_argb,
               int src_stride_argb,
               uint8_t* dst_uyvy,
               int dst_stride_uyvy,
               int width,
               int height);			  

#endif /* HW_SCALER_H */