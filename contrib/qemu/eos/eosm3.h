#ifndef HW_EOSM3_H

#define HW_EOSM3_H

#include "qemu/osdep.h"
#include "hw/hw.h"
#include "qemu/log.h"
#include "eos.h"
#include "mpu.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define PHYSW_DPRINTF(fmt, ...) DPRINTF("[PhySw] ", EOS_LOG_MPU, fmt, ## __VA_ARGS__)
#define PHYSW_EPRINTF(fmt, ...) EPRINTF("[PhySw] ", EOS_LOG_MPU, fmt, ## __VA_ARGS__)
#define PHYSW_VPRINTF(fmt, ...) VPRINTF("[PhySw] ", EOS_LOG_MPU, fmt, ## __VA_ARGS__)
#define PHYSW_DPRINTF0(fmt, ...) DPRINTF("",      EOS_LOG_MPU, fmt, ## __VA_ARGS__)
#define PHYSW_EPRINTF0(fmt, ...) EPRINTF("",      EOS_LOG_MPU, fmt, ## __VA_ARGS__)

//-------------------------------------------------------------------
#define RBF_MAX_NAME        64

//-------------------------------------------------------------------
// Format of header block for each character in the 'font_data' array
// This is immediately followed by 'size' bytes of character data.
typedef struct {
    unsigned char charcode[2];      // Don't change this to a short as the data is not aligned in memory
    unsigned char offset;
    unsigned char size;
} FontData;


// Header as seperate structure so it can be directly loaded from the font file easily
// structure layout maps to file layout - do not change !
typedef struct {
    int magic1, magic2;         // header magic numbers to identify correct font file
    char name[RBF_MAX_NAME];    // name of font (max 64 characters)
    int charSize;               // # of bytes used to store each character
    int points;                 // font size in points
    int height;                 // font height in pixels
    int maxWidth;               // width of widest character
    int charFirst;              // first character #
    int charLast;               // last character #
    int _unknown4;              // ?
    int _wmapAddr;              // offset in font file of wTable array
    int _cmapAddr;              // offset in font file of cTable array
    int descent;                // font descent (not used)
    int intline;                // interline spacing (not used)
} font_hdr;

typedef struct _font {
    font_hdr hdr;

    // calculated values (after font is loaded)
    int charCount;              // count of chars containing in font
    int width;                  // font element width in pixels

    // Width table
    // List of character widths. Elements of list is width of char
    char wTable[256];

    // Character data
    // List of chars. Element of list is a bytecode string, contains pixels representation of char
    char *cTable;

    // Current size of the cTable data
    int cTableSize;
    int cTableSizeMax;                // max size of cTable (max size currently allocated)
} font;





#define FONT_EXPAND(pix)       (((pix) << 20) & FONT_EXPAND_MASK) /* range: -4 ... +3 pixels per character */

#define FONT(font,fg,bg)        ( 0 \
        | ((font) & (0xFFFF0000)) \
        | ((bg) & 0xFF) << 8 \
        | ((fg) & 0xFF) << 0 \
)

/* font by ID */
#define FONT_DYN(font_id,fg,bg) FONT((font_id)<<16,fg,bg)

/* should match the font loading order from rbf_font.c, rbf_init */
#define FONT_MONO_12  FONT_DYN(0, 0, 0)
#define FONT_MONO_20  FONT_DYN(1, 0, 0)
#define FONT_SANS_23  FONT_DYN(2, 0, 0)
#define FONT_SANS_28  FONT_DYN(3, 0, 0)
#define FONT_SANS_32  FONT_DYN(4, 0, 0)

#define FONT_CANON    FONT_DYN(7, 0, 0) /* uses a different backend */

/* common fonts */
#define FONT_SMALL      FONT_MONO_12
#define FONT_MED        FONT_SANS_23
#define FONT_MED_LARGE  FONT_SANS_28
#define FONT_LARGE      FONT_SANS_32

/* retrieve fontspec fields */
#define FONT_ID(font) (((font) >> 16) & 0x7)
#define FONT_BG(font) (((font) & 0xFF00) >> 8)
#define FONT_FG(font) (((font) & 0x00FF) >> 0)
#define FONT_GET_TEXT_WIDTH(font)    (((font) >> 22) & 0x3F0)
#define FONT_GET_EXPAND_AMOUNT(font) (((font) >> 20) & 0x7)



/* retrieve fontspec fields */
#define FONT_ID(font) (((font) >> 16) & 0x7)
#define FONT_BG(font) (((font) & 0xFF00) >> 8)
#define FONT_FG(font) (((font) & 0x00FF) >> 0)
#define FONT_GET_TEXT_WIDTH(font)    (((font) >> 22) & 0x3F0)
#define FONT_GET_EXPAND_AMOUNT(font) (((font) >> 20) & 0x7)


#define FG_COLOR(f) FONT_FG(f)
#define BG_COLOR(f) FONT_BG(f)
#define MAKE_COLOR(fg,bg) FONT(0,fg,bg)

#define FONT_CONDENSED 0x00100000 /* this bit is not (yet) used, so we use it as internal flag */

// #define draw_char(x,y,c,h) do{}while(0)
#define MAX_DYN_FONTS 7

#define BMP_W_PLUS 720
#define BMP_W_MINUS 0
#define BMP_H_PLUS 480
#define BMP_H_MINUS 0

#define COLOR_WHITE             0x01 // Normal white
#define COLOR_BLACK             0x02

void physw_send_keypress(EOSState *, int );
unsigned int eos_handle_gpio_physw( unsigned int , EOSState *, unsigned int , unsigned char , unsigned int );
unsigned int eos_handle_sio_subcpu ( unsigned int , EOSState *, unsigned int , unsigned char , unsigned int );
unsigned int eos_handle_rtc2 ( unsigned int , EOSState *, unsigned int , unsigned char , unsigned int  );

void font_draw(uint32_t , uint32_t , uint32_t , uint32_t , char *);
void mzrm_send(EOSState *);

uint8_t clip_yuv2(int);
uint8_t yuv_to_r2(uint8_t, int8_t);
uint8_t yuv_to_g2(uint8_t, int8_t, int8_t);
uint8_t yuv_to_b2(uint8_t, int8_t);
void draw_pixel_std(unsigned int , unsigned int );
void rgba_to_yuv8b(unsigned int , unsigned int );	
int colormult(int, int, int);

int ARGBToUYVY(const uint8_t* src_argb,
               int src_stride_argb,
               uint8_t* dst_uyvy,
               int dst_stride_uyvy,
               int width,
               int height);	


// int rbf_font_load(char *, font* , int );
// int rbf_font_height(font *);
// int rbf_char_width(font *, int );
int rbf_str_width(font *, const char *);
uint32_t font_by_name(char *, uint32_t , uint32_t );
font *new_font(void);

extern uint32_t kbd_state[];
extern uint8_t rtc2_reg[];

#endif /* HW_EOSM3_H */