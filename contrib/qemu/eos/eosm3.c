#include "hw/eos/eosm3.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "eos.h"
// #include "scaler.h"

#define LOG_MZRM 0
#define LOG_MZRM_JD 0

uint32_t kbd_state[3] = { 0, 0, 0 };
uint8_t rtc2_reg[128];

uint8_t *base_bmp;
uint8_t *base_opac;
uint8_t *base_rgba;

const char *mzrmmsgs[]={
"(000)",
"(001)",
"GraphicSystemCoreSetAutoFinish",
"GraphicSystemCoreFinish",
"CreateZicoLogCtx",
"GraphicLogSetEnableCore",
"InitializeMzrmZtoMSnd",
"GraphicLogSetZicoLogTx",
"(008)",
"(009)",
"InitializeGvdnSystem",
"InitializeGvdnSystemEXT",
"TerminateGvdnSystem",
"GvdnGetFreeResouceState",
"GvdnMakeCurrent",
"GvdnCreateContext",
"GvdnDestroyContext",
"GvdnFinish",
"GvdnSetOstReq",
"GvdnSetCommonContext",
"GvdnCreatePbufferFromClientBuffer",
"GvdnGetGldaWorkAreaInfo",
"GvdngetGldaWorkPeakSize",
"GvdnWinOpenDisplay",
"GvdnWinCloseDisplay",
"GvdnWinQueryDisplay",
"GvdnWinQueryWindow",
"GvdnWinGetError",
"GvdnGetCurDispWindowHandle",
"GvdnGetDrawRequest",
"GvdnUtilDrawRadialGradient",
"(031)",
"(032)",
"InitializeXimr",
"TerminateXimr",
"XimrExe",
"XimrExeGain",
"(037)",
"(038)",
"SflwWrpInitializeCore",
"SflwWrpFinalize",
"SflwWrpLoadFontOnMem",
"SflwWrpUnloadFontCore",
"SflwWrpSetFontSolidColorCore",
"SflwWrpSetRenderingQualityCore",
"SflwWrpSetFontCore",
"SflwWrpSetFontSizeCore",
"SflwWrpSetFontStyleCore",
"SflwWrpSetStringAnchorCore",
"SflwWrpGetCurrentPoint",
"SflwWrpGetFontMetrics",
"SflwWrpSetComplexOrSimpleMode",
"SflwWrpSetPriorityFontList",
"SflwWrpClearPriorityFontList",
"SflwWrpCacheString",
"SflwWrpFixedCacheString",
"SflwWrpClearGlyphCache",
"SflwWrpClearFixedGlyphCache",
"SflwWrpEnableGlyphCache",
"SflwWrpGetStringLength",
"SflwWrpGetRenderingLength",
"SflwWrpDrawString",
"SflwWrpGetCharNumWithinLength",
"SflwWrpDrawStringWithinLength",
"SflwWrpDrawStringWithinRect",
"SflwWrpGetFontSizeWithinHeight",
"SflwWrpGetHeightWithinCurrentFontSize",
"SflwWrpGetCharIndices",
"SflwWrpReleaseCharIndices",
"SflwWrpGetLibVersionNumber",
"SflwWrpIsExistGlyphIndex",
"SflwWrpSetvgGlyphMatrix",
"SflwWrpSetvgPaint",
"SflwWrpGetFontLoadLibraryHandle",
"SflwWrpSetFontStdHeight",
"SflwWrpSetFontKerning",
"SflwWrpGetStringMinYMaxY",
"SflwWrpSetCharacterGap",
"SflwWrpSetFontStyleInfo",
"SflwWrpSetScissoringArea",
"SflwWrpResetScissoringArea",
"SflwWrpRenderStringCore",
"SflwWrpRenderStringWithinLengthCore",
"SflwWrpRenderStringWithinRectCore",
"SflwWrpSetExCmapCore",
"SflwWrpUnsetExCmapCore",
"SflwWrpSetCacheSize",
"SflwWrpGetCharIndicesArray",
"SflwWrpGetAllErrCore",
"SflwWrpGetErrStat",
"SflwWrpGetMakeCurrentErr",
"SflwWrpFillRect",
"SflwWrpSetColorFactorEnableCore",
"SflwWrpSetColorFactorTableCore",
"SflwWrpSetSolidColorAlphaCore",
"SflwWrpSetEdgeColorAlphaCore",
"SflwWrpDrawStringOnArc",
"SflwWrpGetFontMetricsEx",
"SflwWrpSetFontOption",
"SflwWrpClearFontOption",
"SflwWrpCheckErrAssertCore",
"SflwWrpSetAlternateMissing",
"SflwWrpGetCharNumWithinLengthR",
"SflwWrpGetRenderingMinYMaxY",
"(104)",
"(105)",
"JediInitializeRenderer",
"JediTerminateRenderer",
"JediDraw",
"JediDrawTween",
"JediFinish",
"JediClear",
"JediAllDestroyHandle",
"JediRenderSetMask",
"JediRenderEnableMask",
"(115)",
"(116)",
"InitializeGrypForNormal",
"TerminateGrypForNormal",
"GrypGetDrvNormalInitStatus",
"GrypSetBurstLength",
"GrypSetDivisionWidth",
"GrypSetNormalWaitType",
"GrypGetNormalWaitType",
"GrypSetYccSrcField",
"GrypSetYccDstField",
"GrypGetImageWidth",
"GrypGetImageHeight",
"GrypFlush",
"GrypFinish",
"GrypSetSrcVramHandle",
"GrypSetDstVramHandle",
"GrypSetArgbMaskIndexNum",
"GrypGetArgbMaskData",
"GrypSetDrawDevice",
"GrypSetVgBlendOnOff",
"GrypSetCropArea",
"GrypGetCropArea",
"GrypGetRevision",
"GrypSetCmdStackAdr",
"GrypSetRectColorMask",
"GrypSetBlend",
"GrypSetGradStep",
"GrypSetFontDataType",
"GrypSetImageDataType",
"GrypSetYccColorMask",
"GrypSetYccBlend",
"GrypSetYccGradStep",
"GrypSetYccBright",
"GrypSetYccYGain",
"GrypSetYccCGain",
"GrypSetYccPosterization",
"GrypSetYccBilinear",
"GrypSetYccMirror",
"GrypSetYccColorSwap",
"GrypDrawDot",
"GrypDrawHLine",
"GrypDrawVLine",
"GrypDrawLine",
"GrypFillRect",
"GrypDrawRect",
"GrypFill3DRect",
"GrypSetIndexErrImage",
"GrypDrawImage",
"GrypCopyArea",
"GrypDrawYccHLine",
"GrypDrawYccVLine",
"GrypDrawYccRect",
"GrypFillYccRect",
"GrypDrawYccImage",
"GrypCopyYccArea",
"GrypSetColorTableBuffer",
"GrypSetDcSpecialPalette",
"GrypSetImageIndexColorTable",
"GrypSetImageIndexColorTableDA",
"GrypSetImageIndexColor",
"GrypGetImageIndexColor",
"GrypSetImageArgbColorTable",
"GrypSetImageArgbColorTableDA",
"GrypGetImageArgbColorTable",
"GrypGetImageArgbColorTableDA",
"GrypSetImageArgbColor",
"GrypGetImageArgbColor",
"GrypSetFontIndexColorTable",
"GrypSetFontIndexColorTableDA",
"GrypSetFontIndexColor",
"GrypGetFontIndexColor",
"GrypSetFontArgbColorTable",
"GrypSetFontArgbColorTableDA",
"GrypGetFontArgbColorTable",
"GrypGetFontArgbColorTableDA",
"GrypSetFontArgbColor",
"GrypGetFontArgbColor",
"GrypSetArgbGain",
"(194)",
"(195)",
"GrypDsWrpCoreSetBilinear",
"GrypDsWrpCoreSetAfUnknownColorID",
"GrypDsWrpCoreSetAfTransparentColorID",
"GrypDsWrpCoreSetFillOver",
"GrypDsWrpCoreSetPattern",
"GrypDsWrpCoreSetDrawOpacityLattice",
"GrypDsWrpCoreSetIconDataType",
"GrypDsWrpCoreDrawSlantingLine",
"GrypDsWrpCoreDrawSlantingLineClipping",
"GrypDsWrpCoreDrawSlantingLineTransparent",
"GrypDsWrpCoreDrawSlantingLineTransparentClipping",
"GrypDsWrpCoreFillVramForEqualPhase",
"GrypDsWrpCoreDrawImageToVramForEqualPhase",
"GrypDsWrpCoreDrawImageToVramForEqualPhaseBrush",
"GrypDsWrpCoreDrawHistogramToVram",
"GrypDsWrpCoreDrawCharacter",
"GrypDsWrpCoreDrawFlatCharacterClipping",
"GrypDsWrpCoreDrawFlatCharacterClippingFront",
"GrypDsWrpCoreTransferReverseToVram",
"GrypDsWrpCoreReverseDraftVram",
"GrypDcWrpCoreSetBilinear",
"GrypDcWrpCoreSetRotateParam",
"GrypDcWrpCoreSetIconDataType",
"GrypDcWrpCoreDrawLine",
"GrypDcWrpCoreFillVram",
"GrypDcWrpCoreDrawImage1bit",
"GrypDcWrpCoreDrawImage8bit",
"GrypDcWrpCoreDrawImage8bitMag",
"GrypDcWrpCoreDrawImage8bitRotate",
"GrypDcWrpCoreCopyVram",
"GrypDcWrpCoreCopyVramWithMask",
"GrypDcWrpCoreFillRect",
"(228)",
"(229)",
"(230)",
"(231)",
"(232)",
"(233)",
"(234)",
"(235)",
"TweenInitializeBackEnd",
"TweenTerminateBackEnd",
"TweenRemoveAllBackEnd"
};

unsigned int eos_handle_rtc2 ( unsigned int parm, EOSState *s, unsigned int address, unsigned char type, unsigned int value )
{
    unsigned int ret = 0;
    const char * msg = 0;
    int msg_arg1 = 0;
    int msg_arg2 = 0;

    static unsigned int last_sio_txdata = 0;
    static unsigned int last_sio_rxdata = 0;
    static unsigned int last_sio_setup1 = 0;
    static unsigned int last_sio_setup2 = 0;
    static unsigned int last_sio_setup3 = 0;

    switch(address & 0xFF)
    {
        case 0x04:
            if((type & MODE_WRITE) && (value & 1))
            {
                static char default_msg[100];
                snprintf(default_msg, sizeof(default_msg),
                    "Transmit: 0x%08X, setup 0x%08X 0x%08X 0x%08X",
                    last_sio_txdata, last_sio_setup1, last_sio_setup2, last_sio_setup3
                );
                msg = default_msg;

                switch(s->rtc.transfer_format)
                {
                    /* CS inactive, do nothing */
                    case RTC_INACTIVE:
                    {
                        assert(0);
                        break;
                    }

                    /* waiting for a command byte */
                    case RTC_READY:
                    {
                        uint8_t cmd = last_sio_txdata & 0x80;
                        uint8_t reg = last_sio_txdata & 0x70;

                        if (cmd)
                            s->rtc.transfer_format = RTC_READ_BURST;
                        else
                            s->rtc.transfer_format = RTC_WRITE_BURST;

                        s->rtc.current_reg = reg;

                        switch(s->rtc.transfer_format)
                        {
                            case RTC_WRITE_BURST:
                                msg = "Initiate WB (%02X)";
                                msg_arg1 = last_sio_txdata;
                                break;

                            case RTC_READ_BURST:
                                msg = "Initiate RB (%02X)";
                                msg_arg1 = last_sio_txdata;
                                break;

                            default:
                                msg = "Requested invalid transfer mode 0x%02X";
                                msg_arg1 = last_sio_txdata;
                                break;
                        }
                        break;
                    }

                    /* burst writing */
                    case RTC_WRITE_BURST:
                        rtc2_reg[s->rtc.current_reg] = last_sio_txdata & 0xFF;
                        msg = "WB %02X <- %02X";
                        msg_arg1 = s->rtc.current_reg;
                        msg_arg2 = last_sio_txdata & 0xFF;
                        s->rtc.current_reg++;
                        break;

                    /* burst reading */
                    case RTC_READ_BURST:
                        last_sio_rxdata = rtc2_reg[s->rtc.current_reg];
                        msg = "RB %02X -> %02X";
                        msg_arg1 = s->rtc.current_reg;
                        msg_arg2 = last_sio_rxdata;
                        s->rtc.current_reg++;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                ret = 0;
            }
            break;

        case 0x0C:
            msg = "setup 1";
            MMIO_VAR(last_sio_setup1);
            break;

        case 0x10:
            msg = "setup 2";
            MMIO_VAR(last_sio_setup2);
            break;

        case 0x14:
            msg = "setup 3";
            MMIO_VAR(last_sio_setup3);
            break;

        case 0x18:
            msg = "TX register";
            MMIO_VAR(last_sio_txdata);
            break;

        case 0x1C:
            msg = "RX register";
            MMIO_VAR(last_sio_rxdata);
            break;
    }

    io_log("RTC2", s, address, type, value, ret, msg, msg_arg1, msg_arg2);
    return ret;
}

unsigned int eos_handle_sio_subcpu ( unsigned int parm, EOSState *s, unsigned int address, unsigned char type, unsigned int value )
{

    /* SIO2 SubCPU handler */

//  RemCPUSw_tx_GPIO_0x163
//const char msg_0x00[] = { 0x00 };
const char reply_0x00[] = { 0xfe };

//  SubCPUVersion
//const char msg_0x16[] = { 0x16, 0x00, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe };
const char reply_0x16[] = { 0xfe, 0xfe, 0x16, 0x04, 0x74, 0x03, 0x43, 0x01 };

//  StartSwitchCheck
//const char msg_0x0e[] = { 0x0e, 0x00, 0xfe, 0xfe };
const char reply_0x0e[] = { 0xfe, 0xfe, 0x0e, 0x00 };


//  Shutdown
//const char msg_0x0f[] = { 0x0f, 0x00, 0xfe, 0xfe };
const char reply_0x0f[] = { 0xfe, 0xfe, 0x0f, 0x00 };

//  Initialize_RemCPU
//const char msg_0x2a[] = { 0x2a, 0x00, 0xfe, 0xfe, 0xfe };
const char reply_0x2a[] = { 0xfe, 0xfe, 0x2a, 0x01, 0x04 };

//  RemCPU_get_startup_mode
//const char msg_0x01[] = { 0x01, 0x00, 0xfe, 0xfe, 0xfe };
// const char reply_0x01[] = { 0xfe, 0xfe, 0x01, 0x01, 0x01 }; // Rec
const char reply_0x01[] = { 0xfe, 0xfe, 0x01, 0x01, 0x02 }; // Play

//  EF Lens disconnect
//const char msg_0x2d[] = { 0x2d, 0x01, 0x00, 0xfe, 0xfe, 0xfe };
// const char reply_0x2d[] = { 0xfe, 0xfe, 0xfe, 0x2d, 0x01, 0x02 };
const char reply_0x2d[] = { 0xfe, 0xfe, 0xfe, 0x2d, 0x00 };

//  pre_set_startup_mode
//const char msg_0x32[] = { 0x32, 0x00, 0xfe, 0xfe, 0xfe };
const char reply_0x32[] = { 0xfe, 0xfe, 0x32, 0x01, 0x02 };

//  ??? lens clean
//const char msg_0x36[] = { 0x32, 0x00, 0xfe, 0xfe, 0xfe };
const char reply_0x36[] = { 0xfe, 0xfe, 0x36, 0x01, 0x00 };

//  RemCPUSwChk
//const char msg_0x03[] = { 0x03, 0x00, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe };
// const char reply_0x03[]={ 0xfe, 0xfe, 0x03, 0x04, 0x00, 0x03, 0x1f, 0x01 }; // Rec
const char reply_0x03[] = { 0xfe, 0xfe, 0x03, 0x04, 0x02, 0x03, 0x1f, 0x01 }; // Play

//  "SubCPURebootRequest
//const char msg_0x2e[] = { 0x2e, 0x05, 0x01, 0x12, 0x34, 0x56, 0x78, 0xfe, 0xfe };
// const char reply_0x2e[] = { 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0x2e, 0x00 };

//  SubCPUMemoryDump
//const char msg_0x1a[] = { 0x1a, 0x04, 0x00, 0x00, 0x00, 0xfa, 0xfe, 0xfe, 0xfe };
// const char reply_0x1a[] = { 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0x1a, 0x01, 0x41 };


    unsigned int ret = 0;
    char msg[100] = "";
    char mod[10];

    snprintf(mod, sizeof(mod), "SIO%i", parm);

    static unsigned int msg_number = 0;
    static unsigned int msg_size = 0;
    static unsigned int msg_byte_number = 0;

    static unsigned int last_sio2_txdata = 0;
    static unsigned int last_sio2_rxdata = 0;
    static unsigned int last_sio2_setup1 = 0;
    static unsigned int last_sio2_setup2 = 0;
    static unsigned int last_sio2_setup3 = 0;
    unsigned int pc = CURRENT_CPU->env.regs[15];

    static char reply[32];

    char remkbd_state = 0;
    if (kbd_state[1] == 0x00010000) remkbd_state = 2;
    if (kbd_state[1] == 0x00020000) remkbd_state = 1;


    switch(address & 0xFF)
    {
        case 0x04:
            if((type & MODE_WRITE) && (value & 1))
            {
                snprintf(msg, sizeof(msg), "Transmit: 0x%08X, setup 0x%08X 0x%08X 0x%08X PC: 0x%08X", last_sio2_txdata, last_sio2_setup1, last_sio2_setup2, last_sio2_setup3, pc );
                if (msg_byte_number == 0)
                {
                    msg_number = last_sio2_txdata & 0xFF;
                    switch(msg_number)
                    {
                        // default:
                        case 0x00:
                        msg_size = sizeof(reply_0x00);
                        memcpy (reply, reply_0x00, msg_size);
                        break;

                        case 0x16:
                        msg_size = sizeof(reply_0x16);
                        memcpy (reply, reply_0x16, msg_size);
                        break;

                        case 0x0e:
                        msg_size = sizeof(reply_0x0e);
                        memcpy (reply, reply_0x0e, msg_size);
                        break;

                        case 0x0f:
                        msg_size = sizeof(reply_0x0f);
                        memcpy (reply, reply_0x0f, msg_size);
                        break;

                        case 0x2a:
                        msg_size = sizeof(reply_0x2a);
                        memcpy (reply, reply_0x2a, msg_size);
                        break;

                        case 0x2d:
                        msg_size = sizeof(reply_0x2d);
                        memcpy (reply, reply_0x2d, msg_size);
                        break;

                        case 0x01:
                        msg_size = sizeof(reply_0x01);
                        memcpy (reply, reply_0x01, msg_size);
                        break;

                        case 0x32:
                        msg_size = sizeof(reply_0x32);
                        memcpy (reply, reply_0x32, msg_size);
                        break;

                        case 0x36:
                        msg_size = sizeof(reply_0x36);
                        memcpy (reply, reply_0x36, msg_size);
                        break;

                        case 0x03:
                        msg_size = sizeof(reply_0x03);
                        memcpy (reply, reply_0x03, msg_size);
                        reply[4] = remkbd_state;
                        break;

                        default:
                        fprintf(stderr, "[SIO2] EOS M3 SubCPU Unknown MSG: 0x%02X\n", msg_number);
                        for  (int i = 0; i < 16; i++)
                            {
                            reply[i] = 0xfe;
                            }
                        msg_size = 16;
                        break;
                    }
                    // fprintf(stderr, "[SIO2] EOS M3 SubCPU MSG: 0x%02X, size: 0x%02X\n", msg_number, msg_size);
                }
                last_sio2_rxdata = reply[msg_byte_number] & 0xFF;
                // fprintf(stderr, "[SIO2] EOS M3 SubCPU MSG: 0x%02X, size: 0x%02X, byte: 0x%02X, TX:0x%02X RX0x%02X\n", msg_number, msg_size, msg_byte_number, last_sio2_txdata, last_sio2_rxdata );
                msg_byte_number++;
                if (msg_byte_number >= msg_size)  msg_byte_number = 0;


            }
            else
            {
                ret = 0;
            }
            break;

        case 0x0C:
            MMIO_VAR(last_sio2_setup1);
            break;

        case 0x10:
            MMIO_VAR(last_sio2_setup2);
            break;

        case 0x14:
            MMIO_VAR(last_sio2_setup3);
            break;

        case 0x18:
            snprintf(msg, sizeof(msg), "TX register");
            MMIO_VAR(last_sio2_txdata = value);
            break;

        case 0x1C:
            snprintf(msg, sizeof(msg), "RX register");
            MMIO_VAR(last_sio2_rxdata);
            break;
    }

    io_log(mod, s, address, type, value, ret, msg, 0, 0);
    return ret;
}

/* from CHDK keyboard.h - only what we use */
enum key_codes
{
    CHDK_KEY_PLAYBACK = 1,
    CHDK_KEY_MENU,
    CHDK_KEY_FACE,
    CHDK_KEY_UP,
    CHDK_KEY_RIGHT,
    CHDK_KEY_DOWN,
    CHDK_KEY_LEFT,
    CHDK_KEY_ZOOM_OUT,
    CHDK_KEY_ZOOM_IN,
    CHDK_KEY_SET,
    CHDK_KEY_DISPLAY,
    CHDK_KEY_SHOOT_FULL,
    CHDK_KEY_SHOOT_FULL_ONLY,
    CHDK_KEY_SHOOT_HALF,
    CHDK_KEY_POWER,
};

/* from CHDK */
typedef struct {
    short grp;
    short hackkey;
    long canonkey;
} KeyMap;


//EOS M3  addr = 0xD20BF4A0
KeyMap keymap[] = {
    { 1, CHDK_KEY_PLAYBACK        ,0x00010000 }, // inverted physw_status[0]
    { 1, CHDK_KEY_SHOOT_FULL      ,0x00040001 },
    { 1, CHDK_KEY_SHOOT_FULL_ONLY ,0x00000001 },
    { 1, CHDK_KEY_SHOOT_HALF      ,0x00040000 },
    { 1, CHDK_KEY_ZOOM_OUT        ,0x00001000 }, // AE Lock / Index (playback zoom out)
    { 1, CHDK_KEY_ZOOM_IN         ,0x00000800 }, // AF Adjust / Magnify (playback zoom in)
    // { 1, CHDK_KEY_VIDEO           ,0x00000008 },
    { 1, CHDK_KEY_MENU            ,0x00000010 },
    { 1, CHDK_KEY_DISPLAY         ,0x00000400 }, // INFO button (lower left)
    { 1, CHDK_KEY_UP              ,0x00000020 },
    { 1, CHDK_KEY_DOWN            ,0x00000040 },
    { 1, CHDK_KEY_RIGHT           ,0x00000080 },
    { 1, CHDK_KEY_LEFT            ,0x00000100 },
    { 1, CHDK_KEY_SET             ,0x00000200 },
    { 1, CHDK_KEY_POWER           ,0x00020000 }, // inverted physw_status[0]
    // { 1, CHDK_KEY_PRINT           ,0x00002000 },
//  { 0, CHDK_KEY_MFN             ,0x00002000 },
    { 1, CHDK_KEY_FACE            ,0x00002000 },  // M-FN
//  addr = 0xD20BF4A0 + 0x38
//  { 0, SD_lock             ,0x20000000 },
    { 0, 0, 0 }
};


/* http://www.marjorie.de/ps2/scancode-set1.htm */
/* to create a group of keys, simply name only the first key from the group */
static struct {
    int scancode;
    uint32_t key_code;
    const char * pc_key_name;
    const char * cam_key_name;
} key_map[] = {
    { 0xE048,   CHDK_KEY_UP,             "Arrow keys",   "Navigation",                   },
    { 0xE04B,   CHDK_KEY_LEFT,                                                           },
    { 0xE050,   CHDK_KEY_DOWN,                                                           },
    { 0xE04D,   CHDK_KEY_RIGHT,                                                          },

    { 0x0039,   CHDK_KEY_SET,            "SPACE",        "SET",                          },

    { 0x0018,   CHDK_KEY_POWER,          "O",            "Power",                        },
    { 0x0032,   CHDK_KEY_MENU,           "M",            "MENU",                         },
    { 0x0019,   CHDK_KEY_PLAYBACK,       "P",            "PLAY",                         },
    { 0x0017,   CHDK_KEY_DISPLAY,        "I",            "INFO/DISP",                    },
    { 0x0021,   CHDK_KEY_FACE,           "F",            "FACE",                         },

    { 0x002A,   CHDK_KEY_SHOOT_HALF,     "Shift",        "Half-shutter"                  },
    { 0x0036,   CHDK_KEY_SHOOT_HALF                                                      },
    { 0x001C,   CHDK_KEY_SHOOT_FULL,     "Enter",        "Full-shutter"                  },
    { 0x000E,   CHDK_KEY_SHOOT_FULL_ONLY,"Bksp",         "Full-shutter only"             },

    { 0xE049,   CHDK_KEY_ZOOM_IN,        "PgUp, PgDn",   "Zoom in/out"                   },
    { 0xE051,   CHDK_KEY_ZOOM_OUT,                                                       },
};

/* returns key codes (to be looked up in keymap) */
static int translate_scancode_2(int scancode, int first_code)
{
    int code = (first_code << 8) | scancode;

    if (code == 0x003B)
    {
        /* special: F1 -> help */
        return 0x00F1F1F1;
    }

    /* lookup key code */
    for (int i = 0; i < COUNT(key_map); i++)
    {
        /* check both press and unpress events */
        if ((key_map[i].scancode       ) == code ||
            (key_map[i].scancode | 0x80) == code)
        {
            return key_map[i].key_code;
        }
    }

    /* not found */
    return -2;
}

static int translate_scancode(int scancode)
{
    static int first_code = 0;

    if (first_code)
    {
        /* special keys (arrows etc) */
        int key = translate_scancode_2(scancode, first_code);
        first_code = 0;
        return key;
    }

    if (scancode == 0xE0)
    {
        /* wait for second keycode */
        first_code = scancode;
        return -1;
    }

    /* regular keys */
    return translate_scancode_2(scancode, 0);
}

static int key_avail(int scancode)
{
    /* check whether a given key is available on current camera model */
    return translate_scancode_2(scancode & 0xFF, scancode >> 8) > 0;
}

static void show_keyboard_help(void)
{
    PHYSW_EPRINTF0("\n");
    PHYSW_EPRINTF("Available keys:\n");

    int last_status = 0;

    for (int i = 0; i < COUNT(key_map); i++)
    {
        if (key_map[i].pc_key_name)
        {
            last_status = key_avail(key_map[i].scancode);
            if (last_status)
            {
                PHYSW_EPRINTF0("- %-12s : %s\n", key_map[i].pc_key_name, key_map[i].cam_key_name);
            }
        }
        else if (last_status && !key_avail(key_map[i].scancode))
        {
            /* for grouped keys, make sure all codes are available */
            PHYSW_EPRINTF("key code missing: %x %x\n", key_map[i].scancode, key_map[i].key_code);
            exit(1);
        }
    }

    PHYSW_EPRINTF0("- F1           : show this help\n");
    PHYSW_EPRINTF0("\n");
}

void physw_send_keypress(EOSState *s, int keycode)
{

    int key = translate_scancode(keycode);
    if (key <= 0)
    {
        PHYSW_DPRINTF0("Key not recognized: %x\n", keycode);
        return;
    }

    if (key == 0x00F1F1F1)
    {
        show_keyboard_help();
        return;
    }

    PHYSW_DPRINTF0("Key event: %x -> %d\n", keycode, key);

    /* look it up in keymap */
    for (int i = 0; keymap[i].canonkey; i++)
    {
        if (key == keymap[i].hackkey)
        {
            /* update our kbd_state */
            /* let's hope the polling loop is fast enough to catch it :) */
            int unpress = keycode & 0x80;
            int grp = keymap[i].grp;
            if (unpress) {
                // PHYSW_EPRINTF("release [%d] %08X\n", grp, (int) keymap[i].canonkey);
                kbd_state[grp] &= ~keymap[i].canonkey;
            } else {
                // PHYSW_EPRINTF("press   [%d] %08X\n", grp, (int) keymap[i].canonkey);
                kbd_state[grp] |= keymap[i].canonkey;
            }
        }
    }
}




static int RBF_HDR_MAGIC1 = 0x0DF00EE0;
static int RBF_HDR_MAGIC2 = 0x00000003;

static font *font_dynamic;
static char *dyn_font_name;
uint32_t dyn_fonts = 0;

static uint32_t fontloaded;
static uint32_t fntcol;        /*  font's color                 */
static uint32_t anchor;        /*  font's anchor                */
static uint32_t scpos_x;       /*  scissoring area start x      */
static uint32_t scpos_y;       /*  scissoring area start y      */
static uint32_t scpos_x1;      /*  scissoring area end x        */
static uint32_t scpos_y1;      /*  scissoring area end y        */


static void font_put_pixel(int x, int y, int c)
{
    if ((x >= scpos_x)&&(x < (scpos_x + scpos_x1))&&(y >= scpos_y)&&(y < (scpos_y + scpos_y1)))
        draw_pixel_std(x + 960*y, c);
}

//-------------------------------------------------------------------
// Return address of 'character' data for specified font & char
static char* rbf_font_char(font* f, int ch)
{
    if (f && (ch >= f->hdr.charFirst) && (ch <= f->hdr.charLast))
    {
        return &f->cTable[(ch-f->hdr.charFirst)*f->hdr.charSize];
    }

    return 0;
}

static void alloc_cTable(font *f) {

    // Calculate additional values for font
    f->width = 8 * f->hdr.charSize / f->hdr.height;
    f->charCount = f->hdr.charLast - f->hdr.charFirst + 1;

    // set width table to default value
    memset(f->wTable, f->width, 256);

    // allocate cTable memory

    // If existing data has been allocated then we are re-using the font data
    // See if it the existing cTable data is large enough to hold the new font data
    // If not free it so new memory will be allocated
    if ((f->cTable != 0) && (f->cTableSizeMax < (f->charCount*f->hdr.charSize))) {
        free(f->cTable);              // free the memory
        f->cTable = 0;                // clear pointer so new memory is allocated
        f->cTableSizeMax = 0;
    }

    // Allocated memory if needed
    if (f->cTable == 0) {
        // Allocate memory from cached pool
        int size = f->charCount*f->hdr.charSize;
        f->cTable = malloc(size);

        // save size
        f->cTableSize = f->charCount*f->hdr.charSize;
        if (f->cTableSizeMax == 0) f->cTableSizeMax = f->cTableSize;    // Save actual size allocated
    }
}


static void font_draw_char(font *rbf_font, int x, int y, char *cdata, int width, int height, int pixel_width, int fontspec) {
    int xx, yy;
    // uint8_t * bmp = bmp_vram();
    // int fg = FG_COLOR(fontspec);
    // int bg = BG_COLOR(fontspec);
    int x0 = fontspec & FONT_CONDENSED ? 1 : 0;

    // draw pixels for font character
    if (cdata)
    {
        for (yy=0; yy<height; ++yy)
        {
            if (y+yy <= BMP_H_MINUS || y+yy >= BMP_H_PLUS)
            {
                break;
            }
            for (xx=x0; xx<pixel_width; ++xx)
            {
                if (cdata[yy*width/8+xx/8] & (1<<(xx%8)))
                    font_put_pixel(x+xx, y+yy, fontspec);
            }
        }
    }
}


//-------------------------------------------------------------------
static int rbf_draw_char(font *rbf_font, int x, int y, int ch, int fontspec) {
    // Get char data pointer
    char* cdata = rbf_font_char(rbf_font, ch);

    font_draw_char(rbf_font, x, y, cdata, rbf_font->width, rbf_font->hdr.height, rbf_font->wTable[ch], fontspec);

    return rbf_font->wTable[ch];
}


//-------------------------------------------------------------------
// Draw a string colored 'c1' with the character at string-position 'c' colored 'c2'.
static int rbf_draw_string_c(font *rbf_font, int x, int y, const char *str, int fontspec1, int c, int fontspec2) {
     int l=0, i=0;

     while (*str) {
          if (*str == '\n')
          {
              l = 0;
              y += rbf_font->hdr.height;
              str++;
              i++;
              continue;
          }
          l+=rbf_draw_char(rbf_font, x+l, y, *str++, (i==c)?fontspec1:fontspec2);
          ++i;
     }
     return l;
}

//-------------------------------------------------------------------
static inline int rbf_font_height(font *rbf_font) {
    return rbf_font->hdr.height;
}
//-------------------------------------------------------------------
static inline int rbf_char_width(font *rbf_font, int ch) {
    return rbf_font->wTable[ch];
}

font *new_font(void) {
    // allocate font from  memory
    font *f = malloc(sizeof(font));
    if (f) {
        memset(f,0,sizeof(font));      // wipe memory
        // return address in memory
        return f;
    }

    // memory not allocated ! should probably do something else in this case ?
    return 0;
}


//-------------------------------------------------------------------
// Read data from  file using buffer and copy to  font memory
static int font_read(FILE* fd, unsigned char *dest, int len)
{
    // Return actual bytes read
    int bytes_read = 0;

    unsigned char *ubuffer = malloc(len);

    if (ubuffer)
    {
        bytes_read += fread(ubuffer, sizeof(uint8_t), len, fd);
        memcpy(dest, ubuffer, len);
        free(ubuffer);
    }

    return bytes_read;
}
//-------------------------------------------------------------------
// Load from from file. If maxchar != 0 limit charLast (for symbols)
static int rbf_font_load(char *file, font* f, int maxchar)
{
    int i;

    // make sure the font has been allocated
    if (f == 0)
    {
        return 0;
    }

    // open file
    FILE * fd = (file != NULL) ? fopen(file, "rb") : NULL;
    if (!fd)
    {
        fprintf(stderr, "*** Error: can not open font file %s ", file);
        return 0;
    }

    // read header
    i = font_read(fd, (unsigned char*)&(f->hdr), sizeof(font_hdr));

    // check size read is correct and magic numbers are valid
    if ((i != sizeof(font_hdr)) || (f->hdr.magic1 != RBF_HDR_MAGIC1) || (f->hdr.magic2 != RBF_HDR_MAGIC2))
    {
        return 0;
    }

    if (maxchar != 0)
    {
        f->hdr.charLast = maxchar;
    }

    alloc_cTable(f);

    // read width table (using uncached buffer)
    memset(&f->wTable[0], 0, sizeof(f->wTable));
    fseek(fd, f->hdr._wmapAddr, SEEK_SET);

    font_read(fd, (unsigned char*)&f->wTable[f->hdr.charFirst], f->charCount);

    // read cTable data (using uncached buffer)
    fseek(fd, f->hdr._cmapAddr, SEEK_SET);
    font_read(fd, (unsigned char*)f->cTable, f->charCount*f->hdr.charSize);

    fclose(fd);

    return 1;
}

uint32_t font_by_name(char *file, uint32_t fg_color, uint32_t bg_color)
{
    /* check if this font was already loaded */
    for(int pos = 0; pos < (int)dyn_fonts; pos++)
    {
        if(!strcmp(file, dyn_font_name))
        {
            /* yeah, so return a new specifier for this id */
            return FONT_DYN(pos, fg_color, bg_color);
        }
    }

    /* was not loaded, try to load */
    char filename[32];
    snprintf(filename, sizeof(filename), "%s.rbf", file);

    void *font = new_font();

    /* load the font here */
    if(!rbf_font_load(filename, font, 0))
    {
        fprintf(stderr, "*** Error  font %s not loaded", filename);
        free(font);
        return 0;
    }

    /* now updated cached font name (not filename) */
    dyn_font_name = malloc(strlen(filename) + 1);
    strcpy(dyn_font_name, file);

    /* and measure font sizes */
    // font_dynamic.bitmap = font;
    // font_dynamic.height = rbf_font_height((void*)font_dynamic.bitmap);
    // font_dynamic.width = rbf_char_width((void*)font_dynamic.bitmap, '0');
    // dyn_fonts++;
    font_dynamic = font;
    // return FONT_DYN(dyn_fonts - 1, fg_color, bg_color);
    return 1;
}


//-------------------------------------------------------------------
int rbf_str_width(font *rbf_font, const char *str) {
    int l=0;
    int maxl = 0;

    // Calculate how long the string is in pixels
    // and return the length of the longest line
    while (*str)
    {
        if (*str == '\n')
        {
            maxl = MAX(l, maxl);
            l = 0;
        }
        else
        {
            l += rbf_char_width(rbf_font, *str);
        }
        str++;
    }
    maxl = MAX(l, maxl);

    return maxl;
}

unsigned char *vectoricons;
int loadicons(char*);
unsigned char* geticon(int,  int);


//-------------------------------------------------------------------
// Return address of 'icon' data for specified icon & dimensions
unsigned char* geticon(int name, int dimensions){

    int icon_data_offset = *(int*)vectoricons;
    int i = 4;

    while (i < icon_data_offset){

        if ((name == *(int*)(vectoricons + i)) && (dimensions == *(int*)(vectoricons + i + 4))){
            return  vectoricons + icon_data_offset + *(int*)(vectoricons + i + 8);
        }

        if (0 == *(int*)(vectoricons + i))
            return 0;

        i+=12;
    };
    return 0;
}

//-------------------------------------------------------------------
int loadicons(char *file) {

    char filename[32];
    int size;
    int bytes_read = 0;

    snprintf(filename, sizeof(filename), "%s", file);

    FILE * fd = (file != NULL) ? fopen(file, "rb") : NULL;
    if (!fd)
    {
        fprintf(stderr, "*** Error: can not open icon file %s \n", file);
        return 0;
    }

    // get file size
    fseek(fd , 0 , SEEK_END);
    size = (int)ftell(fd);
    // fprintf(stderr, "The file is %d bytes long.\n", size);
    rewind (fd);

    if (!fd)
    {
        fprintf(stderr, "*** Error: icon file %s is empty\n", file);
        fclose(fd);
        return 0;
    }

    vectoricons = malloc(size);

    bytes_read = fread(vectoricons, sizeof(uint8_t), size, fd);

    fclose(fd);
    return bytes_read;
}

uint8_t clip_yuv2(int v) {
    if (v<0) return 0;
    if (v>255) return 255;
    return v;
}

uint8_t yuv_to_r2(uint8_t y, int8_t v) {
    return clip_yuv2(((y<<12) +          v*5743 + 2048)>>12);
}

uint8_t yuv_to_g2(uint8_t y, int8_t u, int8_t v) {
    return clip_yuv2(((y<<12) - u*1411 - v*2925 + 2048)>>12);
}

uint8_t yuv_to_b2(uint8_t y, int8_t u) {
    return clip_yuv2(((y<<12) + u*7258          + 2048)>>12);
}

void draw_pixel_std(unsigned int offset, unsigned int cl)
{
    // DIGIC 6, drawing on 16bpp YUV overlay

    // int active_buffer_index =  active_bitmap_buffer & 1;
    // unsigned char *obu = (unsigned char *)(&opacity_buffer[active_buffer_index][0]);
    unsigned int *rbu = (unsigned int *)(&base_rgba[0]);

    // if ((cl & 0xFF000000) || (cl != 0x00800080))
    // if (cl)
        if (cl == 0x00800080) cl = 0;
        rbu[offset] = cl;
}

int yuv8b_to_rgba(unsigned char* );

int yuv8b_to_rgba(unsigned char *yuv )
{
    uint8_t col[4];
    uint8_t U;
    uint8_t V;
    uint8_t Y;

    if((unsigned long)(yuv) & 2){
        U = *(yuv-2) - 0x80;
        V = *(yuv) - 0x80;
        Y = *(yuv+1);
    }
    else {
        U = *(yuv) - 0x80;
        V = *(yuv+2) - 0x80;
        Y = *(yuv+1);
    }
        col[0] = yuv_to_r2(Y,U);
        col[1] = yuv_to_g2(Y,V,U);
        col[2] = yuv_to_b2(Y,V);

    return  *(unsigned int *)col;
}
/*
void rgba_to_yuv8b(unsigned int offset, unsigned long cl)
{
    // DIGIC 6, drawing on 16bpp YUV overlay

    // int active_buffer_index =  active_bitmap_buffer & 1;
    // unsigned char *obu = (unsigned char *)(&opacity_buffer[active_buffer_index][0]);
    unsigned char *bbu = (unsigned char *)(&base_bmp[0]);
    unsigned char *obu = (unsigned char *)(&base_opac[0]);
    unsigned int y;
    // unsigned int u;
    // unsigned int v;
    // unsigned int o;

    unsigned int y1;
    unsigned int u1;
    unsigned int v1;    
    
    unsigned int A;
    unsigned int R;
    unsigned int G;
    unsigned int B;
    
    unsigned int A1;
    unsigned int R1;
    unsigned int G1;
    unsigned int B1;

         A1 = (cl >> 56) & 0xFF;
         R1 = (cl >> 48) & 0xFF;
         G1 = (cl >> 40) & 0xFF;
         B1 = (cl >> 32) & 0xFF;

        A = (cl >> 24) & 0xFF;
        R = (cl >> 16) & 0xFF;
        G = (cl >>  8) & 0xFF;
        B = (cl      ) & 0xFF;
        
    y =  (0.257 * R) + (0.504 * G) + (0.098 * B) + 16;
    // v =  (0.439 * R) - (0.368 * G) - (0.071 * B) + 128;
    // u = -(0.148 * R) - (0.291 * G) + (0.439 * B) + 128;

    y1 =  (0.257 * R1) + (0.504 * G1) + (0.098 * B1) + 16;
    
    R1 = (R + R1)/2;
    G1 = (G + G1)/2;
    B1 = (B + B1)/2;
    
    v1 =  (0.439 * R1) - (0.368 * G1) - (0.071 * B1) + 128;
    u1 = -(0.148 * R1) - (0.291 * G1) + (0.439 * B1) + 128;
    
    obu[offset*2] = A;
    obu[offset*2+1] = A1;
    


        // CALC_YUV_CHROMA_FOR_COLOR(cl,u,v);
        // bbu[offset*4+0] = (u + u1)/2; // U?
        bbu[offset*4+0] = u1; // U?
        bbu[offset*4+1] = y; // Y
                   
        // bbu[offset*4+2] = (v + v1)/2; // V?
        bbu[offset*4+2] = v1; // V?
        bbu[offset*4+3] = y1; // Y
    
}
*/

void rgba_to_yuv8b(unsigned int offset, unsigned int cl)
{
    // DIGIC 6, drawing on 16bpp YUV overlay

    // int active_buffer_index =  active_bitmap_buffer & 1;
    unsigned char *bbu = (unsigned char *)(&base_bmp[0]);
    unsigned char *obu = (unsigned char *)(&base_opac[0]);
    unsigned int y;
    unsigned int u;
    unsigned int v;
    // unsigned int o;
  
    unsigned int A;
    unsigned int R;
    unsigned int G;
    unsigned int B;
  
        A = (cl & 0xFF000000) >> 24;
        R = (cl & 0x00FF0000) >> 16;
        G = (cl & 0x0000FF00) >> 8;
        B = (cl & 0x000000FF);
    
    // y =  (0.257 * R) + (0.504 * G) + (0.098 * B) + 16;
    // v =  (0.439 * R) - (0.368 * G) - (0.071 * B) + 128;
    // u = -(0.148 * R) - (0.291 * G) + (0.439 * B) + 128;
    
    /*  using BT.709 color space */
    y =  (0.2126  * R) + (0.7152  * G) + (0.0722  * B) + 0;
    v =  (0.615   * R) - (0.55861 * G) - (0.05639 * B) + 128;
    u = -(0.09991 * R) - (0.33609 * G) + (0.436   * B) + 128;    
  
    obu[offset] = A;
      register unsigned int offs2 = (offset>>1)<<2;
    if (offset&1) // x is odd
    {
        bbu[offs2+3] = y; // Y
    }
    else // x is even
    {
  
        // CALC_YUV_CHROMA_FOR_COLOR(cl,u,v);
        bbu[offs2+1] = y; // Y
        bbu[offs2+0] = u; // U?
        bbu[offs2+2] = v; // V?
  
    }
      
}

int colormult(int colv, int col, int strokecol){

    int A1;
    int A2;
    int A3;
    int SW;
    int CW;
    int R2;
    int R3;
    int G2;
    int G3;
    int B2;
    int B3;
    
    
    A1 = (colv & 0xFF000000) >> 24;
    SW = (colv & 0xFF0000) >> 16;
    CW = (colv & 0x00FF00) >> 8;
    // B1 = (colv & 0x0000FF);

    A2 = (col & 0xFF000000) >> 24;
    R2 = (col & 0xFF0000) >> 16;
    G2 = (col & 0x00FF00) >> 8;
    B2 = (col & 0x0000FF);

    A3 = (strokecol & 0xFF000000) >> 24;
    R3 = (strokecol & 0xFF0000) >> 16;
    G3 = (strokecol & 0x00FF00) >> 8;
    B3 = (strokecol & 0x0000FF);

    
    if (CW)
    {
        R2 = (R2 * (CW+1)) >> 8;
        G2 = (G2 * (CW+1)) >> 8;
        B2 = (B2 * (CW+1)) >> 8;   
        return ((((A1*A2)/255) << 24) + ((R2) << 16) + ((G2) << 8) + (B2)); 
    }
    else  if (SW)
    {
        // return colv;
        R3 = (R3 * (SW+1)) >> 8;
        G3 = (G3 * (SW+1)) >> 8;
        B3 = (B3 * (SW+1)) >> 8;
        return ((((A1*A3)/255) << 24) + ((R3) << 16) + ((G3) << 8) + (B3)); 
    }
    if (((R2+G2+B2)/3) < ((R3+G3+B3)/3))
    {
        R2 = R3 - R2;
        G2 = G3 - G2;
        B2 = B3 - B2;
    }
    // A1 = 256;
   
    return (((A1*A2*A3)>> 16) << 24) + (((R3*R2)>> 8) << 16) + (((G3*G2)>> 8) << 8) + ((B3*B2)>> 8);
}


void mzrm_send(EOSState *s)
{
        uint32_t stringlen;
        char MZRM_str[100];
        uint32_t pos_x;          /* rectangle for text string   */
        uint32_t pos_y;          /* rectangle for text string   */
        uint32_t pos_x1;         /* rectangle for text string   */
        uint32_t pos_y1;         /* rectangle for text string   */

        uint32_t time;
        uint32_t MZRM_mbufbase;  /* base address of MZRM buffer */
        uint32_t MZRM_mbuf;      /* address of current message  */
        uint32_t MZRM_mtype;     /* type of current message     */
        uint32_t MZRM_mcount;    /* counter of messages         */
        uint32_t MZRM_size;      /* size of current message     */

        uint32_t pRGBAbuf;       /* pointer to RGBA buffer      */
        uint32_t RGBAbuf;        /* address of RGBA buffer      */
        uint32_t BITMAPbuf;      /* address of Bitmap buffer    */
        uint32_t OPACbuf;        /* address of Opacity buffer   */

        uint32_t MZRM_JD[0x500]; /* buffer for JediDraw message */

        hwaddr len_bmp;          /* lenght of Bitmap buffer     */
        hwaddr len_opac;         /* lenght of Opacity buffer    */
        hwaddr len_rgba;         /* lenght of RGBA buffer       */
        len_bmp = 736 * 480 * 2;
        len_opac = 736 * 480;
        len_rgba = 960 * 480 * 4;

        eos_mem_read(s, 0xBFF00418, &MZRM_mbufbase, 4);
        eos_mem_read(s, 0xBFF00410, &MZRM_mcount, 4);
        eos_mem_read(s, MZRM_mbufbase + (MZRM_mcount * 4), &MZRM_mbuf, 4);
        eos_mem_read(s, MZRM_mbuf, &MZRM_mtype, 4);
        eos_mem_read(s, MZRM_mbuf + 8, &MZRM_size, 4);

#if LOG_MZRM
        fprintf(stderr, "\n%s -> Message: %04d: "KYLW"%s"KRESET", size %04d, address 0x%08x\n", eos_get_current_task_name(s), MZRM_mtype , mzrmmsgs[MZRM_mtype], MZRM_size, MZRM_mbuf);
        uint32_t mw;
        uint32_t n;
        for (n=0; n<(MZRM_size>>2)+3; n++) // +3 words, msg header, not counted in size
        {
            if ((n & 7) == 0)
                fprintf(stderr, "\n%08x: ", MZRM_mbuf + n*4);
            eos_mem_read(s, MZRM_mbuf + n*4, &mw, 4);
            fprintf(stderr, "%08x ", mw);
        }
        fprintf(stderr, "\n");
#endif

        if(MZRM_mtype == 41)  /* SflwWrpLoadFontOnMem */
        {
#if LOG_MZRM
    fprintf(stderr, "SflwWrpLoadFontOnMem\n");
#endif
            char file[] = "argnor33";
            char iconfile[] = "vecticon.dat";
            if (!fontloaded){
                fontloaded = font_by_name(file, COLOR_BLACK, COLOR_WHITE);
                loadicons(iconfile);
            }
        }

        if(MZRM_mtype == 43)  /* SflwWrpSetFontSolidColorCore */
        {
            eos_mem_read(s, MZRM_mbuf + 3*4, &fntcol, 4);
        }

        if(MZRM_mtype == 48)  /* SflwWrpSetStringAnchorCore */
        {
            eos_mem_read(s, MZRM_mbuf + 3*4, &anchor, 4);
        }

        if(MZRM_mtype == 60)  /* SflwWrpGetRenderingLength */
        {
            eos_mem_read(s, MZRM_mbuf + 5*4, &stringlen, 4);
            eos_mem_read(s, stringlen + 4, &MZRM_str[0], MZRM_size - 12);
            uint32_t rendlen = (rbf_str_width(font_dynamic, MZRM_str)) << 16;
            eos_mem_write(s, stringlen , &rendlen, 4);
#if LOG_MZRM
            fprintf(stderr, "RenderingLength [%s] %04d\n", MZRM_str, rendlen >> 16);
#endif
        }

        if(MZRM_mtype == 64) /* SflwWrpDrawStringWithinRect */
        {

            eos_mem_read(s, MZRM_mbuf + 9*4, &stringlen, 4);
            eos_mem_read(s, MZRM_mbuf + 4*4, &pos_x, 4);
            eos_mem_read(s, MZRM_mbuf + 5*4, &pos_y, 4);
            eos_mem_read(s, MZRM_mbuf + 6*4, &pos_x1, 4);
            eos_mem_read(s, MZRM_mbuf + 7*4, &pos_y1, 4);
            eos_mem_read(s, MZRM_mbuf + 0x30, &MZRM_str[0], stringlen + 1);

            pos_x = (pos_x >> 16);
            pos_y = (pos_y >> 16);
            pos_x1 = (pos_x1 >> 16);
            pos_y1 = (pos_y1 >> 16);

            int dfonth = rbf_font_height(font_dynamic);
            int dstrw = rbf_str_width(font_dynamic, MZRM_str);
#if LOG_MZRM
            fprintf(stderr, "%s -> SflwWrpDrawStringWithinRect [%04d,%04d:%04d,%04d]: "KGRN"%s"KRESET" \n",
                            eos_get_current_task_name(s), pos_x, pos_y, pos_x1, pos_y1, MZRM_str);
#endif
        if (anchor == 1)
            pos_x += (pos_x1 / 2) - (dstrw / 2 );
        if (anchor == 2)
            pos_x += pos_x1 - dstrw;
        pos_y += (pos_y1 / 2) - (dfonth / 2);


        eos_mem_read(s, MZRM_mbuf + 3*4, &pRGBAbuf, 4);
        eos_mem_read(s, pRGBAbuf + 4, &RGBAbuf, 4);
        base_rgba = cpu_physical_memory_map(RGBAbuf, &len_rgba, 0);
#if LOG_MZRM
        fprintf(stderr, "pRGBAbuf: 0x%08x, RGBAbuf: 0x%08x, base_bmp: 0x%08x\n", pRGBAbuf, RGBAbuf, *base_bmp);
#endif

        // font_draw((pos_x >> 16), (pos_y >> 16), fntcol, 2, MZRM_str); //0xCC73
        rbf_draw_string_c(font_dynamic, pos_x, pos_y, MZRM_str, fntcol, fntcol,fntcol);
        cpu_physical_memory_unmap(base_rgba, len_rgba, 0, 0);
        }

        if(MZRM_mtype == 79) /* SflwWrpSetScissoringArea */
        {

            eos_mem_read(s, MZRM_mbuf + 6*4, &scpos_x, 4);
            eos_mem_read(s, MZRM_mbuf + 7*4, &scpos_y, 4);
            eos_mem_read(s, MZRM_mbuf + 8*4, &scpos_x1, 4);
            eos_mem_read(s, MZRM_mbuf + 9*4, &scpos_y1, 4);

            scpos_x = (scpos_x >> 16);
            scpos_y = (scpos_y >> 16);
            scpos_x1 = (scpos_x1 >> 16);
            scpos_y1 = (scpos_y1 >> 16);

#if LOG_MZRM
            fprintf(stderr, "%s -> SflwWrpSetScissoringArea [%03d,%03d-%03d,%03d]\n",
                            eos_get_current_task_name(s), scpos_x, scpos_y,
                            scpos_x1, scpos_y1);
#endif

        }

        if(MZRM_mtype == 108)  /* JediDraw */
        {
            int jdlen;                                                   /*  lenght of JediDraw context   */
            int jdaddr;                                                  /*  address of JediDraw context  */
            eos_mem_read(s, MZRM_mbuf + 4*4, &jdaddr, 4);
            eos_mem_read(s, MZRM_mbuf + 6*4, &jdlen, 4);
            eos_mem_read(s, jdaddr, &MZRM_JD[0], jdlen);
            int jw = (MZRM_JD[18] & 0x0000FFFF);                         /*  object's width               */
            int jh = (MZRM_JD[18] & 0xFFFF0000) >> 16;                   /*  object's height              */
            int jx = MZRM_JD[(MZRM_JD[4] - jdaddr)/4];                   /*  scissoring area start x      */
            int jy = MZRM_JD[(MZRM_JD[4] - jdaddr)/4 + 1];               /*  scissoring area start y      */
            int jx1 = MZRM_JD[(MZRM_JD[4] - jdaddr)/4 + 2];              /*  scissoring area end x        */
            int jy1 = MZRM_JD[(MZRM_JD[4] - jdaddr)/4 + 3];              /*  scissoring area end y        */
            int col = MZRM_JD[37];                                       /*  object's color               */
            int fx = *(float*)&MZRM_JD[16];                              /*  object's position x          */
            int fy = *(float*)&MZRM_JD[17];                              /*  object's position y          */
            int strokew = MZRM_JD[25] >> 16;                             /*  stroke's width               */
            int stroketype = MZRM_JD[31] >> 8;                           /*  stroke's type                */
            int strokecol = (MZRM_JD[26] >> 8) + (MZRM_JD[26] << 24);    /*  stroke's color               */
            int obj = MZRM_JD[2];                                        /*  vector object's address      */
            int objtype = MZRM_JD[0] >> 16;                              /*  object's type                */
            int offtype = (MZRM_JD[0] & 0x00000F00) >> 8;                /*  offset's type                */
            int overwrite = (MZRM_JD[32] & 0x00000F00) >> 8;             /*  drawing type (alpha)         */

            int rgbabufw, rgbabufh;                                      /*  drawing buffer's dimensions  */
            eos_mem_read(s, MZRM_mbuf + 11*4, &rgbabufw, 4);
            eos_mem_read(s, MZRM_mbuf + 12*4, &rgbabufh, 4);
            len_rgba = rgbabufw * rgbabufh * 4;                          /*  drawing buffer's lenght      */


            uint16_t obj_buf[64];

            uint8_t *pbitmapobj = 0;
            uint32_t bitmapobj = MZRM_JD[40];;                           /*  bitmap object's address      */
            signed int offx;                                             /*  vector object's offset x     */
            signed int offy;                                             /*  vector object's offset y     */
            int bw = (MZRM_JD[39] & 0x0000FFFF);                         /*  bitmap object's width        */
            int bh = (MZRM_JD[39] & 0xFFFF0000) >> 16;                   /*  bitmap object's height       */
            unsigned char* icon = 0;                                     /*  prerendered icon's address   */

            // if ((obj == 0xfd8a5060) /*|| (obj == 0xfd8a6dd8) || (obj == 0xfd8a7da0)*/) 
                // obj = 0xFD82D41C;                                        /*  solid rectangle              */

            if (objtype == 0x00C0){
                bw = jw;
                icon = geticon(obj, MZRM_JD[18] + ((strokew & 0xF) << 28 ) + ((stroketype & 0x3) << 12 ));
                if (!MZRM_JD[23])
                    col = 0xff3c3c3c;
            }
            else if (objtype == 0x0000)
                obj = bitmapobj;


#if LOG_MZRM_JD
            for (int n=0; n < (jdlen/4); n++)
            {
                if ((n & 7) == 0)
                    fprintf(stderr, "\n%08x: ",(jdaddr + 4*n));
                fprintf(stderr, "%08x ", MZRM_JD[n]);
            }
            fprintf(stderr, "\n");
#endif

            eos_mem_read(s, 0x845C, &time, 4);
#if LOG_MZRM_JD
            fprintf(stderr, "%05d ", time);

            if (obj == 0xFD82D41C)
            fprintf(stderr, "JediDraw "KGRN"wh[%03d,%03d] at[%03d,%03d-%03d,%03d] xy[%04d,%04d]"KRESET" s:%d col:%08x-%08x obj: "KBLU"0x%08x"KRESET"\n", jw, jh, jx, jy, jx1, jy1, fx, fy, strokew, col, strokecol, obj );
            else
            fprintf(stderr, "JediDraw "KGRN"wh[%03d,%03d] at[%03d,%03d-%03d,%03d] xy[%04d,%04d]"KRESET" s:%d col:%08x-%08x obj: 0x%08x %d\n", jw, jh, jx, jy, jx1, jy1, fx, fy, strokew, col, strokecol, obj, offtype );

#endif
            if((!icon) && (obj != 0xFD82D41C))
                fprintf(stderr, "Missing icon "KRED"0x%08x, 0x%08x,"KRESET" \n",obj, MZRM_JD[18] + ((strokew & 0xF) << 28 ) + ((stroketype & 0x3) << 12 ));


            if ((obj == 0xFD881138) || (obj == 0xFD88136C) || (obj == 0xFD8815AC) || (obj == 0xFD881800)){
               col = 0xFFF0F0F0;
            }

            if ((objtype == 0x00C0) && (stroketype != 3) && (strokew != 0)){
                // fx = fx + strokew;
                fy = fy + strokew/2;
                jw = jw + strokew*2;
                jh = jh + strokew*2;
            }
            if ((offtype > 1) && (offtype < 10) && (objtype == 0x00C0))
            {
                eos_mem_read(s, MZRM_JD[2], &obj_buf[0], 128);
                offx = (signed int)( (float)(obj_buf[obj_buf[1]/2 + 0x1A/2] - obj_buf[obj_buf[1]/2 + 0x18/2]) / 20u + 0.5);
                offy = (signed int)( (float)(obj_buf[obj_buf[1]/2 + 0x1E/2] - obj_buf[obj_buf[1]/2 + 0x1C/2]) / 20u + 0.5);

#if LOG_MZRM_JD
                for (int n=0; n < 48; n++)
                {
                    if ((n & 7) == 0)
                        fprintf(stderr, "\n%04x: ",n);
                    fprintf(stderr, "%04x ", obj_buf[n]);
                }
                fprintf(stderr, "\n");
#endif


                switch(offtype)
                {
                    case 2:
                        fx = fx + offx/2 - jw/2;
                    break;

                    case 3:
                        fx = fx + offx - jw;
                    break;

                    case 4:
                        fy = fy + offy/2 - jh/2;
                    break;

                    case 5:
                        fx = fx + offx/2 - jw/2;
                        fy = fy + offy/2 - jh/2;
                    break;

                    case 6:
                        fx = fx + offx - jw;
                        fy = fy + offy/2 - jh/2;
                    break;

                    case 7:
                        fy = fy + offy - jh;
                    break;

                    case 8:
                        fx = fx + offx/2 - jw/2;
                        fy = fy + offy - jh;
                    break;

                    case 9:
                        fx = fx + offx - jw;
                        fy = fy + offy - jh;
                    break;

                    default:
                    break;

                }
#if LOG_MZRM_JD
            fprintf(stderr, "corrected xy[%04d,%04d]; oxoy[%04d,%04d]\n", fx, fy, offx, offy);
#endif
            }

         
            
            int indx = 0;
            int colv = 0;
            hwaddr bitmapobj_len = 0;


            if (objtype == 0){
                bitmapobj_len = bh * bw * 2;
                pbitmapobj = cpu_physical_memory_map(bitmapobj, &bitmapobj_len, 0);
#if LOG_MZRM_JD
                fprintf(stderr, "bitmapobj: 0x%08x, pbitmapobj: 0x%08x, col: 0x%08x\n", bitmapobj, *pbitmapobj, col);
#endif
            }

            eos_mem_read(s, MZRM_mbuf + 3*4, &pRGBAbuf, 4);
            eos_mem_read(s, pRGBAbuf + 4, &RGBAbuf, 4);
            base_rgba = cpu_physical_memory_map(RGBAbuf, &len_rgba, 0);
#if LOG_MZRM_JD
            fprintf(stderr, "JediDraw RGBAbuf: 0x%08x, base_rgba: %p, len_rgba: 0x%lx\n", RGBAbuf, base_rgba, len_rgba);
#endif
            if(((jw <= rgbabufw)&&(jh <= rgbabufh)&&(jx <= rgbabufw)&&(jy <= rgbabufh)&&(jx1 <= rgbabufw)&&(jy1 <= rgbabufh)&&(fx <= rgbabufw)&&(fy <= rgbabufh)) && (objtype != 0x40))
            {
                for (signed int dy = 0; dy < jh; dy++)
                {
                    for (signed int dx = 0; dx < ((bw > jw)?bw:jw); dx++)
                    {
                            colv = 0;
                            if (icon){
                                colv = *(int*)(icon + indx);
                                indx +=4;
                            }

                            /* draw prerendered icon */
                            if (colv /* && (col != 0x00800080) */ ){
                                   int tempcol = colormult(colv, col, strokecol);
                                   // if (obj == 0xfd8a6dd8)
                                       // fprintf(stderr, "%08x>%08x ",colv, col2);
                                   if ((tempcol != 0x00800080) || overwrite) 
                                        draw_pixel_std(fx+dx + rgbabufw*(fy+dy), tempcol);
                                    // draw_pixel_std(fx+dx + rgbabufw*(fy+dy), colv);
                            }

                            /* draw bitmap */
                            if (objtype == 0){ //40
                                    // col = (jw/2 - dx/2) | ((jh/2 - dy/2) << 8) | ((jh/2 - dy/2) << 16);
                                    if (dx < jw){
                                        col = yuv8b_to_rgba(pbitmapobj + indx*2);
                                       draw_pixel_std(fx+dx + rgbabufw*(fy+dy), (col | 0xFF000000));
                                    }
                                    indx++;
                            }

                            /* draw other vector object */
                            if (((fx+dx) >= jx) && ((fx+dx) < (jx + jx1)) && ((fy+dy) >= jy) && ((fy+dy) < (jy + jy1)))
                            {
                                if ((dx < 1) || (dx >= (jw - 1)) || (dy < 1) || (dy >=(jh - 1)) || (obj == 0xFD82D41C)){


                                    if ((!icon) && (objtype != 0) && overwrite ){
                                            draw_pixel_std(fx+dx + rgbabufw*(fy+dy), col );
                                    }
                                }
                            }

                    }
                }
                /* draw stroke (border) */
                if ((!icon) && (strokew))
                {
                    for (signed int dy = 0; dy < jh; dy++)
                    {
                        for (signed int dx = 0; dx < jw; dx++)
                        {
                            if ((dx < strokew) || (dx >= (jw-strokew))  || (dy < strokew) || (dy >=(jh-strokew)))
                            
                            draw_pixel_std(fx+dx + rgbabufw*(fy+dy), strokecol);
                        }
                    }
                }
            }

            cpu_physical_memory_unmap(base_rgba, len_rgba, 0, 0);
            if (objtype == 0)
                cpu_physical_memory_unmap(pbitmapobj, bitmapobj_len, 0, 0);

        }

        if(MZRM_mtype == 35)  /* XimrExe */
        {
            eos_mem_read(s, MZRM_mbuf + 15*4, &BITMAPbuf, 4);
            eos_mem_read(s, MZRM_mbuf + 16*4, &OPACbuf, 4);
            eos_mem_read(s, MZRM_mbuf + 46*4, &RGBAbuf, 4);

            base_rgba = cpu_physical_memory_map(RGBAbuf, &len_rgba, 0);
            base_bmp = cpu_physical_memory_map(BITMAPbuf, &len_bmp, 0);
            base_opac = cpu_physical_memory_map(OPACbuf, &len_opac, 0);

#if LOG_MZRM
            fprintf(stderr, "XMIRexe: RGBA: 0x%08x -> BITMAP: 0x%08x\n", RGBAbuf, BITMAPbuf);
            fprintf(stderr, "XMIRexe: s->disp.bmp_vram: 0x%08x\n", s->disp.bmp_vram);
#endif

            int index_rgba = 0;
            int index_bmp = 0;

            for (int dy = 0; dy < 480; dy++)            /* valid only for LCD display */
            {
                for (int dx = 0; dx < 960; dx++)        /* valid only for LCD display */
                {
                    if (dx < 736){
                        rgba_to_yuv8b(index_bmp, *(int*) &base_rgba[index_rgba]);
                        // base_opac[index_bmp] = base_rgba[index_rgba+3];
                        index_bmp++;
                    }
                    index_rgba +=4;
                }
            }
            
            // ARGBToUYVY(base_rgba, 960 * 4, base_bmp, 736 * 2, 736, 480);
            
            // memcpy(base_bmp, base_rgba, len_bmp);

            cpu_physical_memory_unmap(base_rgba, len_rgba, 0, 0);
            cpu_physical_memory_unmap(base_bmp, len_bmp, 0, 0);
            cpu_physical_memory_unmap(base_opac, len_opac, 0, 0);
        }

        // stringlen = 1;
        // eos_mem_write(s, 0x0000CF08 , &stringlen, 4);
        // eos_mem_write(s, 0x0000CF0C , &stringlen, 4);

        if ((MZRM_mtype == 3) || (MZRM_mtype == 4) || (MZRM_mtype == 33) 
            || (MZRM_mtype == 35) || (MZRM_mtype == 41) || (MZRM_mtype == 60)
            || (MZRM_mtype == 116) || (MZRM_mtype == 110) || (MZRM_mtype == 117))
                eos_trigger_int(s, 0x145, 1);
}

